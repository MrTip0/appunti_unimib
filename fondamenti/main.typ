#set document(
  title: "Appunti di fondamenti dell'informatica",
  author: "Nicolò Luigi Allegris",
)
#set page(paper: "a4", numbering: "1/1")
#set text(font: "Roboto", size: 11pt)

#set heading(numbering: "1.")

#show outline.entry.where(level: 1): it => {
  v(12pt, weak: true)
  strong(it)
}

#show heading.where(level: 1): it => {
  pagebreak()
  it
}

#show heading.where(level: 2): it => {
  v(10pt)
  it
}

#align(center + horizon)[
  #text(size: 40pt)[Fondamenti dell'informatica]\
  A.A. 2023/2024
]

#outline(title: "Indice", indent: auto, depth: 2)

#include "fondamenti.typ"

#include "numeri.typ"

#include "insiemi.typ"