
= Fondamenti dell'informatica

Fondamenti #sym.arrow.r Matematica del discreto #sym.arrow.r Composto da numeri
distinti

== Dato
Un dato può essere
- Qualitativo #sym.arrow.r Linguaggio naturale
- Quantitativo
  - Discreto
  - Continuo

== Matematica del discreto
/ Insieme Discreto: Composto da elementi isolati $(NN "/" ZZ)$.
/ Insieme Continuo: Infiniti elementi senza spazi vuoti $(RR)$.

/ Si occupa di:
- Classificare #sym.arrow.r trovare caratteristiche comuni in entità diverse.
- Enumerare #sym.arrow.r assegnare ad ogni oggetto un modello di calcolo per
  consentirne l'indicizzazione.
- Combinare #sym.arrow.r studiare insiemi finiti per permutare e combinare gli
  elementi.

/ Sono tutti accomunati: dalla costruzione di algoritmi.

== Logica
Ossia la disciplina che studia la verità, ossia offre strumenti per definire
delle verità oppure delle falsità.