= Teoria dei numeri

== I numeri naturali
- Sono: $1, 2, 3, 4, 5, ..., n, n+1$.\
- Sono innati, infatti nascono dall'azione di contare, ossia dal passare da un
  numero al suo successore.\
- Vengono rappresentati dal simbolo $NN$ (N graziata).\
- Sono discreti
- $inf NN = 0 = min NN$

=== Proprietà dei numeri naturali
- $min NN = 0$
- Ogni elemento ha un suo successore S(N)
// - $forall n in NN, exists S(n) = n+1$
- 0 non è successore di alcun numero
// - $exists.not n in NN | S(n) = 0$
- 2 elementi diversi hanno successivi diversi

=== Processo di induzione
Una proprietà è vera in tutto $NN$ sse (se e solo se):
- è vera in $0$
- se è vera in $n$ allora è vera in $S(n)$

== Numeri interi relativi: ($ZZ$)
ossia numeri naturali preceduti da un segno $(+/-)$
- $+ -> "positivo"$
- $- -> "negativo"$
Se il segno di due numeri interi è uguale ($+1, +2$) si dicono concordi,
altrimenti ($-1, +2$) sono discodi.

Valore assoluto _(o modulo)_ di un numero intero è lo stesso valore privato del
segno.
$
|n| = cases(n "se" n >= 0\
-n "se" n < 0)
$

=== Proprietà di $ZZ$
- Non ha minimo ne massimo
- Ogni elemento ha sia successivo che predecessore

== Altri insiemi numerici
/ Razionali: $(QQ = p/q)$\
/ Irrazionali: $II$, non si possono esprimere come frazione, ad es: $sqrt(2)$\
/ Reali: $RR = QQ union II$

$ NN subset ZZ subset QQ subset RR subset CC $
