= Teoria degli insiemi
Un insieme (o raccolta, o collezzione, o aggregato) è un concetto innato o
primitivo e definisce un insieme di elementi raggruppati da una caratteristica
comune ben definita. Ad esempio: "Gli alunni che hanno superato l'esame di
Fondamenti dell'informatica nel corso dell'a.a. 2023/2024" è un insieme, mentre "Gli
alunni che hanno superato brillantemente l'esame di Fondamenti dell'informatica
nel corso dell'a.a. 2023/2024" non è un insieme in quanto non è chiaro da che
voto venga considerato brillante l'esame, in questo caso, brillantemente è
un'ambiguità.

/ Legenda:
- Lettera maiuscola (A-Z) #sym.arrow.r nome di insieme
- Lettera minuscola (a-z) #sym.arrow.r elemento di un insieme

== Rappresentazioni degli insiemi
Esistono 2 modi per rappresentare un insieme: Estensionale ed Intensionale.

=== Rappresentazione estensionale
La rappresentazione estensionale (o per elencazione) di un insieme è l'elenco di
tutti gli elementi appartenenti a un insieme racchiuso tra graffe ed elencati
una sola volta.\
Es: $S = {a, b, c} = {a, b, c, a}$

=== Rappresentazione intensionale
La rappresentazione intensionale di un insieme è la rappresentazione tramite la
proprietà $P$ comune di un determiato insieme: $S = {x|P}$.\
Es: $A = {x|x in NN and x < 4} = {0, 1, 2, 3}$

== Il simbolo ($=$)
Il simbolo "$=$" indica che i 2 elementi posti alla destra e alla sinistra del
simbolo sono uguali.

Un insieme $A$ e un insieme $B$ sono uguali sse per ogni elemento di $A$ compare
in $B$ e viceversa.

=== Proprietà
- $A = A$: Riflessività
- $A = B "sse" B = A$: Simmetria
- se $A = B$ e $B = C$ allora $A = C$: Transitività

== Il simbolo ($in$)
Il simbolo "$in$" indica che l'elemento a sinistra del simbolo fa parte
dell'insieme alla sua destra.

=== Appartenenza e intensionabilità
- Se $a$ rispetta $P$ allora $a in A$
- Ogni elemento di $A$ rispetta $P$

== Il simbolo ($subset$)
Il simbolo $subset$ significa che l'insieme a sinistra è parte dell'insieme a
destra del segno.\
Es: $A = {1, 2, 3, 4}, B = {2, 4} => B subset A$

$A = emptyset and B subset A => B = emptyset$ (impropio)\
$A = {a} and B subset.eq A => B = emptyset or B = {a} = A$ (impropio)\

=== Proprietà
- $S subset.eq S$: Riflessività
- se $S_1 subset.eq S_2 and S_2 subset.eq S_3$ allora $S_1 subset.eq S_3$:
  Transitività
- se $S_1 subset.eq S_2 and S_2 subset eq S_1$ allora $S_1 = S_2$: Anti simmetria

== Insieme $cal(P)(A)$
L'insieme potenza, o insieme delle parti di $A$ è l'insieme di tutti i possibili
sottoinsiemi di $A$.

$cal(P)(A)$ contiene sempre $2^(|A|)$ elementi.\
Es:
$
A = {1, 2, 3} \
cal(P)(A) = {emptyset, {1}, {2}, {3}, {1, 2}, {1, 3}, {2, 3}, {1, 2, 3}}
$

== Unione
L'unione di due insiemi $S$ e $T$ è definita in questo modo:
$
S union T = {x|x in A or x in B}
$

=== Proprietà
- $S union S = S$: Idempotenza
- $S_1 union S_2 = S_2 union S_1$: Commutatività
- $S union emptyset = S$: Elemento neutro $(emptyset)$
- $S_1 union S_2 = S_2 "sse" S_1 subset.eq S_2$: Assorbimento
- $(S_1 union S_2) union S_3 = S_1 union (S_2 union S_3)$: Associatività
- $S_1 subset.eq S_1 union S_2$, $S_2 subset.eq S_1 union S_2$: Monotonicità

== Intersezione
L'intersezione di due insiemi $S$ e $T$ è definita in questo modo:
$
S sect T = {x|x in A and x in B}
$

=== Proprietà
- $S sect S = S$: Idempotenza
- $S_1 sect S_2 = S_2 sect S_1$: Commutatività
- $S sect emptyset = emptyset$: Annichilazione $(emptyset)$
- $S_1 sect S_2 = S_1 "sse" S_1 subset.eq S_2$: Assorbimento
- $(S_1 sect S_2) sect S_3 = S_1 sect (S_2 sect S_3)$: Associatività
- $(S_1 sect S_2) subset.eq S_1$, $(S_1 sect S_2) subset.eq S_2$: Monotonicità

== Proprietà distributiva dell'unione e dell'intersezione
$ S_1 sect (S_2 union S_3) = (S_1 sect S_2) union (S_1 sect S_3) $
$ S_1 union (S_2 sect S_3) = (S_1 union S_2) sect (S_1 union S_3) $

== Complementazione di un insieme
Dato $U$ l'insieme universo, la differenza tra $S subset.eq U$ e $U$ si chiama
complemento di $S$ in $U$, o solo complemento di $S$ qualora $U$ fosse
sottointeso. Si denota come $overline(S)$ ed è definito da:
$ overline(S) = {x|x in U and x in.not S} $

=== Proprietà
- $overline(overline(S)) = S$
- $overline(U) = emptyset$
- $overline(emptyset) = U$
- $overline(S_1 union S_2) = overline(S_1) sect overline(S_2)$ (Legge di de
  Morgan)
- $overline(S_1 sect S_2) = overline(S_1) union overline(S_2)$ (Legge di de
  Morgan)
- $S union overline(S) = U$
- $S sect overline(S) = emptyset$
- $S_1 = S_2 "sse" overline(S_1) = overline(S_2)$
- $S_1 subset.eq S_2 "sse" overline(S_2) subset.eq overline(S_1)$

== Differenza tra due insiemi
Si denota la differenza tra un insieme $S$ e un insieme $T$ come $S - T$ o $S\\T$ e
si definisce come:
$ S-T = {x|x in S and x in.not T} $

=== Proprietà
- $S-S = emptyset$
- $S-emptyset = S$
- $emptyset - S = emptyset$
- $(S_1 - S_2)-S_3 = (S_1 - S_3)-S_2 = S_1-(S_2-S_3)$
- $S_1 - S_2 = S_1 sect overline(S_2)$

== Differenza simmetrica
La differenza simmetrica tra un insieme $S$ e un insieme $T$ si denota come $S Delta T$ e
si definisce come:
$ S Delta T = (S-T) union (T-S) $
e sono tutti gli elementi che compaiono in uno solo dei due elementi.

=== Proprietà
- $S Delta S = emptyset$
- $S Delta emptyset = S$
- $S_1 Delta S_2 = S_2 Delta S_1$
- $S_1 Delta S_2 = (S_1 union S_2)-(S_1 sect S_2)$

== Famiglie di insiemi
Quando abbiamo un insieme i cui elementi sono tutti insiemi, lo chiamiamo
*famiglia di insiemi*.\
Data $cal(F)$ una famiglia di insiemi:\
L'unione è definita come:
$ union cal(F) = {x|x "appartiene a un qualche elemento di" cal(F)} $
L'intersezione è definita come:
$ sect cal(F) = {x|x "appartiene a tutti gli elementi di" cal(F)} $

=== Partizioni di insiemi
Dato $S eq.not emptyset$ si chiama partizione di $S$ la famiglia $cal(F)$ che
rispetta le seguenti proprietà:
+ Ogni elemento di $S$ appartiene a un qualche elemento di $cal(F)$
+ Ogni elemento di $S$ compare in uno e un solo insieme di $cal(F)$
+ Nessun elemento di $cal(F)$ deve essere vuoto
oppure:
+ $union cal(F) = S$
+ $forall A,B in cal(F), A eq.not B, A sect B = emptyset$
+ $exists.not A in cal(F) | A = emptyset$

Es:
$ S = {q, w, e, r, t, y} $ e siano:
#columns(2, [
  $S_1 = {q, w, e}$\
  $S_2 = {r, t, y}$\
  $S_3 = {r, t}$\
  #colbreak()
  $S_4 = {y}$\
  $S_5 = {}$\
])

- $cal(F_1) = {S_1, S_4}$ non è una partizione
- $cal(F_2) = {S_1, S_2, S_5}$ non è una partizione
- $cal(F_3) = {S_1, S_2}$ è una partizione
- $cal(F_4) = {S_1, S_3, S_4}$ è una partizione

Dati $P$ i numeri pari e $D$ quelli dispari: ${P, D}$ è una partizione di $NN$

== Insieme ordinato
Una coppia ordinata è composta da 2 elementi in cui è possibile distinguere il
primo dal secondo e si denota come:
$ <x, y> $

=== Proprietà
$ <x,y> = <z,t> " sse " x = z and y = t $

=== Notazione insiemistica
$ <x,y> = {{x}, {x,y}} $

=== N-Upla
$ <x_1, x_2, ..., x_n> $

== Prodotto Cartesiano
Dati $A eq.not emptyset and B eq.not emptyset$, il prodotto cartesiano tra $A$ e $B$ è
definito come:
$ A times B = {<x,y>| x in A and y in B} $
se uno dei due insiemi è uguale a $emptyset$ allora il prodotto cartesiano è a
sua volta uguale a $emptyset$
NB.
$ A times B eq.not B times A $
=== Potenza di sistemi
$ "se" A = B "allora" A times B = A^2 = B^2 $
$S^n$ è l'insieme di tutte le $n$-uple composte dagli elementi di $S$

== Sequenza
Dato un insieme $S$, $sigma$ è una sequenza finita di elementi di $S$ se $sigma = <s_1, s_2, ..., s_n>, n in N and s_i in S$

=== Segmento di una sequenza
Un segmento di una sequenza finita $sigma = <s_1, ..., s_n>$ è una sequenza $sigma' = <s_k, s_(k+1),..., s_(m-1), s_m>, 1 <= k <= m <= n$,
se $k = 1$ allora $sigma'$ si chiama segmento iniziale.

== Relazioni
$ forall R in cal(P)(S times T) R "può essere considerato una Relazione di " S "e " T $

=== Relazione Binaria
Una relazione binaria $R$ tra $2$ insiemi $S$ e $T$ è un insieme di coppie
ordinate $<x,y>$ dove $x in S and y in T | R subset.eq S times T$

$ "dom"(R) = {x in A | exists y in B, <x,y> in R} $

$ "codom"(R) = {y in B | exists x in A, <x,y> in R} $

Il campo è invece $"dom"(R) union "codom"(R)$

=== Proprietà delle relazioni
Una relazione $R$ definita su un insieme $X (X^2)$ può avere le seguenti
proprietà:
- $"riflessiva se" <x,x> in R, forall x in X", equivalentemente se" I_X subset R$
- $"simmetrica se" <x,y> in R => <y,x> in R", equivalentemente se" R = R^t$
- $"antisimmetrica se" <x,y> in R and <y,x> in R => x=y", equivalentemente se" R sect R^t subset I_X$
- $"transitiva se" <x,y> in R and <y,z> in R => <x,z> in R$

=== Operazioni sulle Relazioni
$ overline(R) = {<x,y>|<x,y> in.not R} subset S times T $
$ R^(-1) = {<y,x>|<x,y> in R} subset T times S $

=== Identità
Dato un insieme $A$, la sua Identità è la relazione:
$I_A = {<x,x> | x in A}$

== Funzioni (o applicazioni)
Sono relazioni tra un insieme $S$ e un insieme $T$ t.c. ad ogni elemento di $S$ si
associa #underline("al più") un solo elemento dell'insieme $T$.

Se $<a,b>,<a,c> in R$ allora $b = c$

Se $forall a in A exists! b in B | <a,b> in R$ allora è una relazione totale.

Sia $f$ una relazione $(f subset S times T)$,
$f$ è una funzione se $forall x in "dom"(f) exists! y | <x,y> in f$

Se $"dom"(f) = S$ allora la funzione è totale, altrimenti è parziale.

Se dato un $x in f$ esiste $f(x)$ allora si dice che $f$ è definita in $x$.

Questioni di notazione: $f subset.eq A times B = f:A->B$ dove $"dom"(f) subset.eq A and "codom"(f) subset.eq B$

$f:S->T$ è iniettiva sse $forall x,y in S, x != y, f(x) != f(y)$

$f:S->T$ è suriettiva sse $forall y in T exists x in S | f(x) = y$ oppure se $"codom"(f) = T$

Una funzione è biettiva sse è iniettiva e suriettiva.

Una funzione è Biunivoca se è biettiva e totale: $forall y in T exists! x in S | f(x) = y$

== Arietà
Sono relazioni con più parametri, quindi l'insieme di partenza è un prodotto
cartesiano tra 2 o più insiemi:
- Unaria: $f: S -> T$
- Binaria: $f: S^2 -> T$
- Ternaria: $f: S^3 -> T$
- N-aria: $f: S^n -> T$

L'arietà è il numero di domini nel prodotto cartesiano.

== Punto fisso
Un punto fisso per una funzione definita da un insieme in sé è un elemento
coincidente con la sua immagine.

Un punto fisso per una funzione $f: S->S$ definita su un insieme $S$ è un
elemento
$x in S$ tale che: $ x = f(x) $

== Funzione Inversa
Una funzione $f: S->T$ ammette una funzione inversa $f^(-1): T->S$ sse $f$ è
iniettiva.

Una funzione $f: A->B$ è invertibile se esiste una funzione $g: B->A$ t.c. $f(g(x)) = x$

=== Proprietà

- $f, g "iniettive" => f circle.small g "e " f circle.small g "sono iniettive"$
- $f, g "suriettive" => f circle.small g "e " f circle.small g "sono suriettive"$
- $f, g "invertibili" => f circle.small g "e " f circle.small g "sono invertibili"$